# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from django.conf import settings
from django.db.models import signals


def avatar_field(instance, filename):
    return os.path.join('users', str(instance.user.pk), filename)


@python_2_unicode_compatible
class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_('Name of User'), blank=True, max_length=255)
    avatar = models.ImageField(upload_to=avatar_field, null=True, blank=True)
    phone = models.CharField('Phone', max_length=20, null=True, blank=True)
    city = models.CharField('City', max_length=45, null=True, blank=True)
    us_state = models.CharField('US State', max_length=45, null=True, blank=True)
    zipcode = models.CharField('Zip', max_length=10, null=True, blank=True)

    group_affiliation = models.BooleanField('Is this person part of a group?', default=False)
    group_affiliation_name = models.CharField('Group Member', max_length=45, null=True, default=False)

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})
