from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from marjoram.orgapp.views import (
    index_view,
    dashboard_view,
    about_view,
    #messages,
    organizations,
)

urlpatterns = [
    url('^$', index_view, name='index'),
    url(r'^dashboard/$', dashboard_view, name='dashboard'),
    url(r'^about/$', about_view, name='about'),
]

# organizations
urlpatterns += [
    url(r'^organizations/filter/(?P<pk>\w+)/$', organizations.organization_detail_view, name='organization_profile'),
    url(r'^organizations/(?P<pk>\w+)/settings/$', organizations.organization_settings_view, name='organization_settings'),
    url(r'^organizations/(?P<pk>\w+)/contact/$', organizations.organization_contact_view, name='contact_organization'),
    url(r'^organizations/filter/$', organizations.organization_list_view, name="list_organization"),
    url(r'^organizations/create/$', organizations.organization_add_view, name='create_organization'),
]
