# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin
from marjoram.orgapp import models

admin.site.register(models.Organization)
