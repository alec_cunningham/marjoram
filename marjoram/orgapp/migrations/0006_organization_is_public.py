# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-10 22:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orgapp', '0005_auto_20160709_0000'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='is_public',
            field=models.BooleanField(default=False),
        ),
    ]
