# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-29 02:31
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orgapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, verbose_name='Name')),
                ('description', models.TextField(blank=True, max_length=1000, verbose_name='Description')),
                ('email', models.EmailField(blank=True, max_length=50, verbose_name='Email')),
                ('address', models.CharField(blank=True, max_length=30, verbose_name='Address')),
                ('phone', models.CharField(blank=True, max_length=250, verbose_name='Phone')),
                ('max_volunteers', models.IntegerField(blank=True, null=True, verbose_name='Max Volunteers')),
                ('min_volunteers', models.IntegerField(blank=True, null=True, verbose_name='Minimum Volunteers')),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=45, verbose_name=b'First Name')),
                ('last_name', models.CharField(blank=True, max_length=45, verbose_name=b'Last Name')),
                ('phone', models.CharField(blank=True, max_length=20, verbose_name=b'Phone')),
                ('address', models.CharField(blank=True, max_length=45, verbose_name=b'Address')),
                ('address2', models.CharField(blank=True, max_length=45, verbose_name=b'Address 2')),
                ('city', models.CharField(blank=True, max_length=45, verbose_name=b'City')),
                ('state', models.CharField(blank=True, max_length=45, verbose_name=b'State')),
                ('zipcode', models.CharField(blank=True, max_length=10, verbose_name=b'Zipcode')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
