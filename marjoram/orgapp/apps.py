from django.apps import AppConfig


class OrgsConfig(AppConfig):
    name = 'marjoram.orgapp'
    verbose_name = "Orgs"
