from __future__ import unicode_literals
from django.db import models
from django.conf import settings


class Group(models.Model):

    name = models.CharField(max_length=50, null=True, blank=True)
    description = models.TextField(max_length=500)
    email = models.EmailField(max_length=50)
    phone = models.CharField(max_length=250)
    members = models.ManyToManyField(settings.AUTH_USER_MODEL, null=True, through='Member')

    def __str__(self):              # __unicode__ on Python 2
        return self.name

    class Meta:
        ordering = ('name',)
        app_label = 'volapp'

class Member(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    group = models.ForeignKey('Group')
    is_admin = models.BooleanField(default=False)

    @property
    def display_name(self):
        return u'{}'.join(self.user.username)

    def __unicode__(self):
        return u'{}, member of {}'.format(
            self.username,
            self.models.Group.name)

    class Meta:
        app_label = 'volapp'
        ordering = ('user__username',)
