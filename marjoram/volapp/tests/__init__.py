from django.test import TestCase
from django.contrib.auth.models import User

class BaseTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='test_user')
        self.user.set_password('foobar')
        self.user.email = 'test_user@email.com'
        self.user.save()
        self.system_user = User.objects.get(username='orgapp_sys')
        for x in range(0,3):
            u = User.objects.create_user(username='test_user_{}'.format(x))
            u.set_password('foobar')
            u.email = '{}@email.com'.format(u.username)
            u.save()

        self.test_users = User.objects.filter(username__startswith="test_user_")

        for x in range(0, 2):
            u = User.objects.create_user(username='members_{}'.format(x))
            u.set_password('foobar')
            u.email = '{}@example.com'.format(u.username)
            u.save()

        self.test_members = User.objects.filter

        self.default_group_kwargs = {
            'creator': self.user,
            'name': 'Test Name',
            'description': 'Test Description',
            'phone': '3333333333',
            'volunteers': self.test_users,
            'members': self.members,}
