from django.http import HttpResponse
from django.views.generic import ListView, TemplateView, View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from marjoram.volapp import models

class IndexView(TemplateView):
    template_name = 'vol/index.html'

    def dispatch(self, request, *args, **kwargs):
        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)


class ExploreView(ListView):
    template_name = 'vol/explore.html'
    model = models.Group

class DashboardView(ListView):
    template_name = 'org/dashboard.html'
    paginate_by = 10
    model = models.Group
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
         return super(DashboardView, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
         context = super(DashboardView, self).get_context_data(*args, **kwargs)
         context['groups'] = models.Group.objects.filter(
             creator=self.request.user,
         )


class JsonView(View):
    content_type = 'application/json'
    status = None

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        response = super(JsonView, self).dispatch(request, *args, **kwargs)
        if isinstance(response, HttpResponse):
            return response
        else:
            return self.render_to_response(response)

    def render_to_response(self, context):
        content = json.dumps(context)
        return HttpResponse(
            content,
            content_type=self.content_type,
            status=self.status,
        )

dashboard_view = DashboardView.as_view()
explore_view = ExploreView.as_view()
index_view = IndexView.as_view()
json_view = JsonView.as_view()
