from django.views.generic import DetailView, ListView
from marjoram.volapp import models

class MemberListView(ListView):
    template_name = 'org/member_list.html'
    paginate_by = 10

member_list = MemberListView.as_view()
