from django.views.generic import DetailView, View, ListView, TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from marjoram.volapp import models
from . import JsonView


class GroupCreateView(CreateView):
    model = models.Group
    fields = []
    template_name = 'vol/create_group.html'

class GroupProfileView(DetailView):
    model = models.Group
    template_name = 'vol/group_detail.html'

class GroupDeleteView(TemplateView):
    pass

class EditGroupView(UpdateView):
    model = models.Group
    template_name = 'vol/group_settings.html'        

# Admin
class GroupListView(ListView):
    template_name = 'vol/group_list.html'
    model = models.Group
    paginate_by = 10

class GroupMemberListView(ListView):
    pass

class GroupMemberAddView(TemplateView):
    pass

class GroupMemberDetailView(DetailView):
    pass

class GroupMemberDeleteView(TemplateView):
    pass

class GroupMemberEditListView(ListView):
    pass

class GroupMemberEditDetailView(UpdateView):
    pass


group_member_list = GroupMemberListView.as_view()
group_member_add = GroupMemberAddView.as_view()
group_member_delete = GroupMemberDeleteView.as_view()
group_member_detail = GroupMemberDetailView.as_view()
group_member_edit_list = GroupMemberEditListView.as_view()
group_member_edit_detail = GroupMemberEditDetailView.as_view()
group_list = GroupListView.as_view()
group_detail = GroupProfileView.as_view()
group_settings = EditGroupView.as_view()
create_group = GroupCreateView.as_view()
delete_group = GroupDeleteView.as_view()


class GroupAPI(JsonView):

    status = 200
    ACTIONS = (
        'search_groups'
    )

    # @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        self.member = False
        return super(GroupAPI, self).dispatch(*args, **kwargs)

    @property
    def default_group_list(self):
        return models.volunteer.Volunteer.objects.get.all()

    def _build_json(self, users, errors, success=True):
        json_dict = {
            'errors': errors,
            'groups': [],
            'success': success
        }
        for group in groups:
            json_dict['groups'].append({
                'name': models.volunteer.Volunteer.objects.get('name'),
            })

            return json_dict

    def _filter_users_by_name(self, users, name):
        return users.filter(
            Q(username__icontains=name) |
            Q(userprofile__display_name__icontains=name)
        )

    def _search_users(self, post_data):
        name = post_data.get('name')
        users = self._filter_users_by_name(
            self.default_volunteer_list,
            name
        )
        return self._build_json(users, {})

    def post(self, *args, **kwargs):
        post_data = self.request.POST.copy()
        action = post_data.get('action')
        if action in self.ACTIONS:
            return getattr(self, '_{}'.format(action))(post_data)

        self.status = 400
        return {
            'errors': {'action': 'Invalid Action'},
            'groups': [],
            'success': False
        }

json_view = GroupAPI.as_view()
