from django.conf.urls import url, include
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from marjoram.volapp.views import (
    index_view,
    explore_view,
    dashboard_view,
    groups,
    members,
)

urlpatterns = [
    url('^', index_view, name='index'),
    url(r'^dashboard/$', dashboard_view, name='dashboard'),
    url(r'^explore/$', explore_view, name='explore'),
]

# Groups
urlpatterns += [
    url(r'^groups/$', groups.group_list, name='group_list'),
    url(r'^groups/(?P<pk>\w+)/$', groups.group_detail, name='group_detail'),
    url(r'^groups/create/$', groups.create_group, name='group_create'),
    url(r'^groups/delete/$', groups.delete_group, name='group_delete'),
]

# Members
urlpatterns += [
    url(r'^groups/(?P<pk>\w+)/members/$', groups.group_member_list, name='group_member_list'),
    url(r'^members/(?P<pk>\w+)/$', groups.group_member_detail, name='group_member_detail'),
    url(r'^groups/members/create/$', groups.group_member_add, name='group_member_add'),
    url(r'^groups/members/delete/$', groups.group_member_delete, name='group_member_delete'),
    url(r'^groups/members/$', groups.group_member_edit_list, name='group_member_edit_list'),
    url(r'^groups/members/(?P<pk>\w+)/edit$', groups.group_member_edit_detail, name='group_member_edit_list'),
]

# Api
urlpatterns += [
    #url(r'^api-view/volunteers/$', volunteers.json_view, name="volunteers_json"),
    url(r'^api-view/groups/$', groups.json_view, name="groups_json"),
]
