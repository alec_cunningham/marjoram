from django import forms
from django.forms import ModelForm
from django.forms.extras import widgets
from django.forms.formsets import formset_factory
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.admin import widgets as admin_widgets
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User, Group, Permission

from marjoram.volapp import models
_APP = 'volapp'

'''
class ManageGroupsForm(ModelForm):
    """Add/edit groups"""
    class Meta:
        model = models.Group
        fields = '__all__'
        widgets = {
            'permissions': admin_widgets.FilteredSelectMultiple(
                verbose_name=_('Permissions'), is_stacked=True),
        }

class ManageUserGroupsForm(forms.Form):
    """Manage User groups"""
    groups = forms.ModelMultipleChoiceField(
        queryset=models.Groups.objects.all(),
        required=False,
        widget=admin_widgets.FilteredSelectMultiple(
            verbose_name=_('Groups'), is_stacked=False)
    )
'''
from .contact import ContactForm
