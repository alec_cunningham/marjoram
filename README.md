## Marjoram

The full Marjoram suite, connecting volunteers and organizations together.

![Build Status](https://travis-ci.org/marjoram/marjoram.svg?branch=master)
[![Documentation Status](https://readthedocs.org/projects/marjoram/badge/?version=latest)](http://marjoram.readthedocs.io/en/latest/?badge=latest)

### Test coverage

To run the tests, check your test coverage, and generate an HTML coverage report

```
  $ coverage run manage.py test
  $ coverage html
  $ open htmlcov/index.html
```

Running tests with py.test

```
  $ py.test
```

### Documentation

All documentation is located on readthedocs, which can be located [here](http://marjoram.rtfd.io/).


### Docker

Learn how to install all the docker tools [here](https://flurdy.com/docs/docker/docker_compose_machine_swarm_cloud.html)

Create the machine:

```
$ docker-machine create --driver virtualbox dev1
```

Get the IP Address:

```
$ docker-machine ip dev1
```

Build the Stack

```
$ docker-compose -f dev.yml build
```
- Omit the f flag in order to build the production environment.

Boot the System

```
$ docker-compose -f dev.yml up
```

To migrate your application and create a superuser, use ```docker-compose run```:

```
$ docker-compose -f dev.yml run django python manage.py migrate
$ docker-compose -f dev.yml run django python manage.py createsuperuser
```

## Documentation

The following is a quick reference to Marjoram's models and views. Enjoy.

### Frontend

```
### pages

* landing
* dashboard
* explore
* contact
* {% inherit account_templates %}
* ...

### volapp

* create_group.html
* group_detail.html
* group_list.html
* group_settings.html
* member_list.html
* index.html
* {% inherit account_templates %} -- add fields
* ...

### orgapp

* create_organization.html
* organization_detail.html
* organization_list.html
* organization_settings.html
* {% inherit account_templates %} -- add owned organizations
```


## Backend
```
### shared

* [done]update_user ->    UserUpdateView
* [done]list_users  ->    UserListView
* [done]redirect_users -> UserRedirectView
* [done]create_user
* [done]update_user -> UserUpdateView
* delete_user
* [done]detail_user->  UserDetailView
```
```
### volapp

* create_group -> GroupCreateView
* update_group -> EditGroupView
* delete_group -> GroupDeleteView
* detail_group -> GroupProfileView
* list_groups ->  GroupListView
* group_member_list ->      GroupMemberListView
* group_member_edit_list -> GroupMemberEditListView
* group_member_edit_detail -> GroupMemberEditDetailView
* group_member_delete ->    GroupMemberDeleteView
* group_member_add ->       GroupMemberAddView
* list_members -> MemberListView

```
```
### orgapp

* create_organization - CreateOrganizationView
* update_organization - UpdateOrganizationView
* delete_organization - OrganizationDelete
* [done]detail_organization - OrganizationDetailView
* [done]list_organizations -  OrganizationListView
```


#### Models


* ```marjoram.volapp.models.GroupMember```
	* ```member -> ForeignKey -> User```
	* ```group -> ForeignKey -> Group```
* ```marjoram.volapp.models.Group```
	* ```members -> ManyToManyField -> User related_name='members'```
* ```marjoram.orgapp.models.Messages```
	* ```sender, recipient, parent_message -> ForeignKey -> User```
* ```marjoram.orgapp.models.Organization```
	* ```creator -> ForeignKey -> User ```
